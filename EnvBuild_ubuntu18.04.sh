#!/bin/bash

echo made catkin_work_space
sudo mkdir -p ./catkin_ws/src
cd catkin_ws/src
catkin_init_workspace
cd ..
catkin_make
echo workspace created

echo imu_tk build
echo Install lie
sudo apt-get install libdw-dev
echo Install ceres-solver1.14 lie
sudo apt-get install liblapack-dev libsuitesparse-dev libcxsparse3.1.2 libgflags-dev libgoogle-glog-dev libgtest-dev
echo Install ceres-solver1.14
cd ..
cp insfile/ceres-solver-1.14.0 -r .
mkdir ceres-bin
cd ceres-bin
cmake ../ceres-solver-1.14.0
make -j8
sudo make install
cd ..
echo ceres-1.14 Install over

echo Install code_utils lie
sudo apt-get install libdw-dev
cp insfile/code_utils-master -r catkin_ws/src
cd catkin_ws
catkin_make
cd src
cp insfile/imu_utils-master -r src
cd ..
catkin_make
echo imu_utils completed
echo imu-tk Install start
echo Install lie
sudo apt-get install build-essential cmake libeigen3-dev libqt4-dev libqt4-opengl-dev freeglut3-dev gnuplot
cd src
cp insfile/imu_tk-master -r .
cd imu_tk-master
mkdir build
cd build
cmake ..
make -j8
cd ..
cd ..
cd ..


echo kalibr lie Install
sudo apt-get install python-setuptools python-rosinstall ipython libeigen3-dev libboost-all-dev doxygen libopencv-dev 
     ros-melodic-vision-opencv ros-melodic-image-transport-plugins ros-melodic-cmake-modules software-properties-common 
     libpoco-dev python-matplotlib python-scipy python-git python-pip ipython libtbb-dev libblas-dev liblapack-dev 
     python-catkin-tools libv4l-dev

sudo apt-get install python-pip
sudo pip install python-igraph

echo create kalibr workspace

mkdir -p kalibr_ws/src
cd kalibr_ws
source /opt/ros/kinetic/setup.bash
cd src
catkin_init_workspace
cd ..
catkin_make
cd src
cp ../../insfile/kalibr-master .
cd .. 
catkin_make
source kalibr_ws/devel/setup.bash


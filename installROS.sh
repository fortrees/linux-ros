#!/bin/bash

echo ready to Install ROS
release_num=$(lsb_release -r --short)
code_name=$(lsb_release -c --short)
hw_arch=$(uname -m)
 
echo "release_num= $release_num"
echo "code_name= $code_name"
echo "hardware Architecture = $hw_arch"

if [ $release_num == "18.04" ];then
   echo ready to Install ROS melodic For ubuntu 18.04
   echo set_source
   sudo sh -c '. /etc/lsb-release && echo "deb http://mirrors.ustc.edu.cn/ros/ubuntu/ $DISTRIB_CODENAME main" > /etc/apt/sources.list.d/ros-latest.list'
   echo Install key
   sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys F42ED6FBAB17C654
   echo Install lie
   sudo apt-get update
   sudo apt-get install ros-melodic-desktop-full
   sudo apt-get install ros-melodic-rqt*
   echo rosdep init
   sudo rosdep init
   rosdep update
else
   echo not suported,beg yourself
fi 
